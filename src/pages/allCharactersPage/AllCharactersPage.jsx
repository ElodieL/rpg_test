import React, { useContext } from 'react';
import CharacterCard from '../../components/characterCard/CharacterCard';
import { CharacterContext } from '../../contexts/characterContext/characterState';

const AllCharactersPage = () => {
    const characterContext = useContext(CharacterContext);
    return ( 
        <div>
            {console.log(characterContext.characters[0].equipments.weapon.name)}
            {console.log(characterContext.characters[0].equipments.weapon)}
            {characterContext.characters.map( character =>
                <CharacterCard key = {character.id}
                    name  = {character.name} 
                    lvl   = {character.lvl}
                    race  = {character.race.name}
                    role  = {character.role.name}
                    hp    = {character.stats.health}
                    mp    = {character.stats.mana}
                    ad    = {character.stats.attackDamages}
                    ap    = {character.stats.magicDamages}
                    armor = {character.stats.armor}
                    rm    = {character.stats.magicResist}
                    rolesAvailables = {character.rolesAvailables}
                    equipmentsAvailables = {character.equipmentsAvailables}
                    weapon = {character.equipments.weapon.name}
                    head = {character.equipments.head.name}
                    body = {character.equipments.body.name}
                    shield = {character.equipments.shield.name}
                    boots = {character.equipments.boots.name}
  
                />
            )}

        </div>
     );
}
 
export default AllCharactersPage;