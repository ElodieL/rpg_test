import React from 'react';
import './CharacterCard.scss';
import PropTypes  from 'prop-types';

const CharacterCard = props => {
    return ( 
        <div className="CharacterCard">
            <h4>Informations:</h4>
            <ul>
                <li>nom: {props.name}</li>
                <li>niveau: {props.lvl}</li>
                <li>race: {props.race}</li>
                <li>classe: {props.role}</li>
            </ul>
            <h4>Statistiques:</h4>
            <ul>
                <li>points de vie: {props.hp}</li>
                <li>points de mana: {props.mp}</li>
                <li>points de force: {props.ad}</li>
                <li>points de magie: {props.ap}</li>
                <li>armure: {props.armor}</li>
                <li>protection magique: {props.rm}</li>
            </ul>
                <h4>equipments</h4>
            <ul>
                <li>arme: {props.weapon}</li>
                <li>tête: {props.head}</li>
                <li>corps: {props.body}</li>
                <li>bouclier: {props.shield}</li>
                <li>chaussures: {props.boots}</li>

            </ul>
            <h4>Classes disponibles pour cette race:</h4>
            <ul>
                {props.rolesAvailables.map(role =>
                    <li key={role.type}>{role.name}</li>
                )}
            </ul>
            <h4>Type d'équipements disponible pour cette classe:</h4>
            <ul>
                {props.equipmentsAvailables.map(equipments =>
                    <li key={equipments.type}>{equipments.name}</li>
                )}
            </ul>
        </div>
     );
};
 
export default CharacterCard;

CharacterCard.propTypes = {
    name: PropTypes.string.isRequired,
    race: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    lvl: PropTypes.number.isRequired,
    hp: PropTypes.number.isRequired,
    mp: PropTypes.number.isRequired,
};

CharacterCard.defaultProps = {
    name: 'sans nom',
    race: 'sans race',
    role: 'sans rôle',
    lvl: 0,
    hp: 0,
    mp: 0,
}