import Character from '../models/CharacterModel';
import * as race from './RaceData';
import { noShield } from './Equipments/shieldData';

const karma = new Character(
    1, 
    'Karma', 
    1, 
    race.elf, 
    race.elf.stats, 
    race.elf.rolesAvailables[0],
    race.elf.rolesAvailables, 
    {
        weapon: race.elf.rolesAvailables[0].equipmentsAvailables[0].equipments[0],
        head: race.elf.rolesAvailables[0].equipmentsAvailables[1].equipments[1],
        body: race.elf.rolesAvailables[0].equipmentsAvailables[2].equipments[0],
        shield: noShield,
        boots: race.elf.rolesAvailables[0].equipmentsAvailables[4].equipments[1],
    },
    race.elf.rolesAvailables[0].equipmentsAvailables, 
    []
);

const figaro = new Character(
    2,
    'Figaro',
    5,
    race.human,
    race.human.stats,
    race.human.rolesAvailables[0],
    race.human.rolesAvailables,
    {
        weapon: race.human.rolesAvailables[0].equipmentsAvailables[0].equipments[0],
        head: race.human.rolesAvailables[0].equipmentsAvailables[1].equipments[1],
        body: race.human.rolesAvailables[0].equipmentsAvailables[2].equipments[0],
        shield: race.human.rolesAvailables[0].equipmentsAvailables[3].equipments[1],
        boots: race.human.rolesAvailables[0].equipmentsAvailables[4].equipments[0],
    },
    race.human.rolesAvailables[0].equipmentsAvailables, 
    [],
    
);

const paulo = new Character(
    3,
    'Paulo',
    10,
    race.ent,
    race.ent.stats,
    race.ent.rolesAvailables[1],
    race.ent.rolesAvailables,
    {
        weapon: race.ent.rolesAvailables[0].equipmentsAvailables[0].equipments[1],
        head: race.ent.rolesAvailables[0].equipmentsAvailables[1].equipments[0],
        body: race.ent.rolesAvailables[0].equipmentsAvailables[2].equipments[1],
        shield: race.ent.rolesAvailables[0].equipmentsAvailables[3].equipments[0],
        boots: race.ent.rolesAvailables[0].equipmentsAvailables[4].equipments[0],
    },
    race.ent.rolesAvailables[0].equipmentsAvailables, 
    [],
);


const ALL_CHARACTERS = [
    karma,
    figaro,
    paulo

];

export default ALL_CHARACTERS;