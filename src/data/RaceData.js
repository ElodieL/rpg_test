import Race from '../models/RaceModel';
import * as raceType from '../constants/racesConstant';
import * as rolesAvailables from '../data/RoleData';

export const human = new Race(
    raceType.HUMAN, 
    'Humain', 
    {
        health: 150, 
        mana: 15, 
        attackDamages: 15, 
        magicDamages: 10, 
        armor: 7, 
        magicResist: 3, 
        speed: 10
    }, [
        rolesAvailables.warrior, 
        rolesAvailables.mage]
);

export const elf = new Race(
    raceType.ELF, 
    'Elfe', { 
        health: 100, 
        mana: 20, 
        attackDamages: 15, 
        magicDamages: 20, 
        armor: 5, 
        magicResist: 3, 
        speed: 12
    }, [
        rolesAvailables.mage, 
        rolesAvailables.pyroman]
);

export const ent = new Race(
    raceType.ENT, 
    'Ent', {
        health: 170, 
        mana: 5, 
        attackDamages: 10, 
        magicDamages: 10, 
        armor: 15, 
        magicResist: 10, 
        speed: 5
    }, [
        rolesAvailables.warrior, 
        rolesAvailables.pyroman]
);