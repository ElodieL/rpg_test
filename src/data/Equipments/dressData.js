import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const whiteDress = new Equipment(1, equipmentTypes.DRESS, 'Robe blanche', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 3,
    magicResist: 3,
    speed: 0
    }, []
);
export const leatherDress = new Equipment(2, equipmentTypes.DRESS, 'Robe de cuir', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 7,
    magicResist: 0,
    speed: 0
    }, []
);