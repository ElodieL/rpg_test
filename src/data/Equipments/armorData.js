import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const leatherShirt = new Equipment(1, equipmentTypes.ARMOR, 'Chemise de cuir', {
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 7,
    magicResist: 0,
    speed: 0
    }, []
);
export const chainMail = new Equipment(2, equipmentTypes.ARMOR, 'Cotte de mailles', {
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 13,
    magicResist: 0,
    speed: 0
    }, []
);