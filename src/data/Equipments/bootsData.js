import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const littleBoots = new Equipment(2, equipmentTypes.BOOTS, 'Petites bottes', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 3,
    magicResist: 0,
    speed: 2
    }, []
);
export const sandals = new Equipment(2, equipmentTypes.BOOTS, 'Sandales', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 2,
    magicResist: 0,
    speed: 4
    }, []
);