import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const tureen = new Equipment(1, equipmentTypes.HELM, 'Soupière', {
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 2,
    magicResist: 0,
    speed: 0
    }, []
);
export const woodenHelmet = new Equipment(2, equipmentTypes.HELM, 'Casque en bois', {
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 5,
    magicResist: 0,
    speed: 0
    }, []
);
export const ironHelmet = new Equipment(3, equipmentTypes.HELM, 'Casque en fer', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 8,
    magicResist: 0,
    speed: 0
    }, []
);