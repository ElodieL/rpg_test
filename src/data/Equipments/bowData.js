import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const woodenBow = new Equipment(1, equipmentTypes.BOW, 'Arc de bois', {
    health: 0, 
    mana: 0,
    attackDamages: 5,
    magicDamages: 0,
    armor: 0,
    magicResist: 0,
    speed: 2
    }, []
);
export const longBow = new Equipment(2, equipmentTypes.BOW, 'Arc long', { 
    health: 0, 
    mana: 0,
    attackDamages: 7,
    magicDamages: 0,
    armor: 0,
    magicResist: 0,
    speed: 2
    }, []
);