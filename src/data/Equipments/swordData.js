import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const longSword = new Equipment(1, equipmentTypes.SWORD, 'Epée longue', {
    health: 0, 
    mana: 0,
    attackDamages: 7,
    magicDamages: 0,
    armor: 0,
    magicResist: 0,
    speed: 0
    }, []
);
export const ironSword = new Equipment(2, equipmentTypes.SWORD, 'Epée de fer', {
    health: 0, 
    mana: 0,
    attackDamages: 14,
    magicDamages: 0,
    armor: 0,
    magicResist: 0,
    speed: 0
    }, []
);