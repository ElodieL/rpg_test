import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const noShield = new Equipment(0, equipmentTypes.SHIELD, 'Cette classe ne peut pas porter de bouclier', {}, []);

export const woodenShield = new Equipment(1, equipmentTypes.SHIELD, 'bouclier de bois', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 5,
    magicResist: 0,
    speed: 2
    }, []
);
export const ironShield = new Equipment(2, equipmentTypes.SHIELD, 'bouclier de fer', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 10,
    magicResist: 0,
    speed: 2
    }, []
);