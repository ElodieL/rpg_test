import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const headband = new Equipment(1, equipmentTypes.TIARA, 'Bandeau', { 
    health: 0, 
    mana: 0,
    attackDamages: 0,
    magicDamages: 0,
    armor: 1,
    magicResist: 2,
    speed: 0
    }, []
);
export const flowerCrown = new Equipment(2, equipmentTypes.TIARA, 'Couronne de fleurs', {
    health: 0, 
    mana: 3,
    attackDamages: 0,
    magicDamages: 0,
    armor: 1,
    magicResist: 2,
    speed: 0
    }, []
);