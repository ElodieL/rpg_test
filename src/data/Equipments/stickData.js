import * as equipmentTypes from '../../constants/itemsConstant';
import { Equipment } from '../../models/EquipmentsModel';

export const woodenStick = new Equipment(1, equipmentTypes.STICK, 'Baguette de bois', {
    health: 0, 
    mana: 0,
    attackDamages: 1,
    magicDamages: 3,
    armor: 0,
    magicResist: 0,
    speed: 0
    }, []
);
export const silverStick = new Equipment(2, equipmentTypes.STICK, 'Baguette d\'argent', {
    health: 0, 
    mana: 0,
    attackDamages: 2,
    magicDamages: 6,
    armor: 0,
    magicResist: 0,
    speed: 0
    }, []
);