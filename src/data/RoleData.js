import Role from '../models/RoleModel';
import * as roleType  from '../constants/rolesConstant';
import * as equipmentAvailable from '../data/EquipmentData';

export const warrior = new Role(roleType.WARRIOR, 'Guerrier', [
    equipmentAvailable.sword,
    equipmentAvailable.helm,
    equipmentAvailable.armor,
    equipmentAvailable.shield,
    equipmentAvailable.boots,
]);
export const mage = new Role(roleType.MAGE, 'Mage', [
    equipmentAvailable.stick,
    equipmentAvailable.tiara,
    equipmentAvailable.dress,
    equipmentAvailable.shield,
    equipmentAvailable.boots,
]);
export const pyroman = new Role(roleType.PYROMAN, 'Pyromane', [
    equipmentAvailable.stick,
    equipmentAvailable.bow,
    equipmentAvailable.helm,
    equipmentAvailable.armor,
    equipmentAvailable.shield,
    equipmentAvailable.boots,
]);

export const ALL_ROLES =  [
    warrior,
    mage, 
    pyroman
];

