import * as equipmentTypes from '../constants/itemsConstant';
import { EquipmentType } from '../models/EquipmentsModel';
import * as helmData from './Equipments/helmData';
import * as tiaraData from './Equipments/tiaraData';
import * as armorData from './Equipments/armorData';
import * as dressData from './Equipments/dressData';
import * as swordData from './Equipments/swordData';
import * as stickData from './Equipments/stickData';
import * as bowData from './Equipments/bowData';
import * as shieldData from './Equipments/shieldData';
import * as bootsData from './Equipments/bootsData';


//Heads
export const helm = new EquipmentType(equipmentTypes.HELM, 'Casques', [
    helmData.tureen,
    helmData.woodenHelmet,
    helmData.ironHelmet,
    
]);
export const tiara = new EquipmentType(equipmentTypes.TIARA, 'Tiares', [
    tiaraData.headband,
    tiaraData.flowerCrown,
]);

//Bodies
export const armor = new EquipmentType(equipmentTypes.ARMOR, 'Armures', [
    armorData.leatherShirt,
    armorData.chainMail,
]);
export const dress = new EquipmentType(equipmentTypes.DRESS, 'Robes', [
    dressData.whiteDress,
    dressData.leatherDress,
]);

//Weapons
export const sword = new EquipmentType(equipmentTypes.SWORD, 'Epées', [
    swordData.longSword,
    swordData.ironSword,
]);
export const stick = new EquipmentType(equipmentTypes.STICK, 'Baguettes', [
    stickData.woodenStick,
    stickData.silverStick,
]);
export const bow = new EquipmentType(equipmentTypes.BOW, 'Arcs', [
    bowData.woodenBow,
    bowData.longBow,
]);


//Equipments
export const head = new EquipmentType(equipmentTypes.HEAD, 'Tête', [helm, tiara]);
export const body = new EquipmentType(equipmentTypes.BODY, 'Corps', [armor, dress]);
export const weapon = new EquipmentType(equipmentTypes.WEAPON, 'Arme', [sword, stick, bow]);
export const shield = new EquipmentType(equipmentTypes.SHIELD, 'Bouclier', [
    shieldData.noShield,
    shieldData.woodenShield,
    shieldData.ironShield,
]);
export const boots = new EquipmentType(equipmentTypes.BOOTS, 'Chaussures', [
    bootsData.littleBoots,
    bootsData.sandals,
]);


export const ALL_EQUIPMENT = [
    head,
    body,
    weapon,
    shield,
    boots,
];