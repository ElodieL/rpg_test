import React from 'react';
import './App.scss';
import Routing from './Routing';
import { CharacterState } from '../contexts/characterContext/characterState';

function App() {
  return (
    <CharacterState>
      <div className="App">
        <Routing/>
      </div>
    </CharacterState>
  );
}

export default App;
