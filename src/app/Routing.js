import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import AllCharactersPage from '../pages/allCharactersPage/AllCharactersPage';

const Routing = () => {
    return ( 
        <>
            <Router>
                <header className="App__header">

                </header>
                <main className="App__main">   
                    <Switch>
                        <Route exact path='/' component={AllCharactersPage}/>
                    </Switch>
                </main>
                <footer className="App__footer">

                </footer>  
            </Router>
        </>
     );
}
 
export default Routing;