class Role {
    constructor(type, name, equipmentsAvailables) {
        this.type = type;
        this.name = name;
        this.equipmentsAvailables = equipmentsAvailables;
    };
};

export default Role;