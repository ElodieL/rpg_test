class Character {
    constructor(id, name, lvl, race, stats, role, rolesAvailables,  equipments, equipmentsAvailables, abilitiesLearned) {
        this.id = id;
        this.name = name;
        this.lvl = lvl;
        this.race = race;
        this.stats = stats;
        this.role = role;
        this.rolesAvailables = rolesAvailables;
        this.equipments = equipments;
        this.equipmentsAvailables = equipmentsAvailables;
        this.abilitiesLearned = abilitiesLearned;
    };
};

export default Character;