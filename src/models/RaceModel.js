class Race {
    constructor(type, name, stats, rolesAvailables) {
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.rolesAvailables = rolesAvailables;
    };
};

export default Race;