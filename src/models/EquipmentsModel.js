export class Equipment {
    constructor(id, type, name, stats, abilities) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.stats = stats;
        this.abilities = abilities;
    };

};

export class EquipmentType {
    constructor(type, name, equipments) {
        this.type = type;
        this.name = name;
        this.equipments = equipments;
    };
};


