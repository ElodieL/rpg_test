export const ON_MYSELF = 'ON_MYSELF';
export const ON_ONE_ALLY = 'ON_ONE_ALLY';
export const ON_ALL_ALLY = 'ON_ALL_ALLY';
export const ON_ONE_ENNEMY = 'ON_ONE_ENNEMY';
export const ON_ALL_ENNEMY = 'ON_ALL_ENNEMY';