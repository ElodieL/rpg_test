import React, { createContext, useReducer } from 'react';
import ALL_CHARACTERS from '../../data/CharacterData';
import { characterReducer } from './characterReducer';

export const CharacterContext = createContext({});

export const CharacterState = props => {
    const [characterList] = useReducer(characterReducer, {characters: ALL_CHARACTERS});

    return ( 
        <CharacterContext.Provider
            value={{
                characters: characterList.characters,
            }}
        >
            {props.children}
        </CharacterContext.Provider>
     );
};
 

